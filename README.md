[SceneBuilder's](https://bitbucket.org/gluon-oss/scenebuilder) splendidly designed color picker tool. As pictures below show, this is just the picker tool by itself which can be easily implemented and customized further in graphics design projects.

![colorpicker2.png](https://bitbucket.org/repo/ypKxddX/images/1568698162-colorpicker2.png)